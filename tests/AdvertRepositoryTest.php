<?php declare(strict_types=1);

namespace App\Tests;

use App\AdvertRepository;
use App\AdvertGateway;
use App\CurrencyConverterInterface;
use PHPUnit\Framework\TestCase;

class AdvertRepositoryTest extends TestCase
{
    public function testAdvertShouldBeFound(): void
    {
        $gateway = $this->getMockBuilder(AdvertGateway::class)
            ->disableOriginalConstructor()
            ->getMock();

        $gateway->expects($this->once())
            ->method('getFromDb')
            ->with(1)
            ->will($this->returnValue([
                'id' => 1,
                'name' => 'AdName_FromMySQL',
                'text' => 'AdText_FromMySQL',
                'price' => 10,
            ]));

        $gateway->expects($this->once())
            ->method('getFromDaemon')
            ->with(2)
            ->will($this->returnValue([
                'id' => 2,
                'name' => 'AdName_FromDaemon',
                'text' => 'AdText_FromDaemon',
                'price' => 10,
            ]));

        $currencyConverter = $this->getMockBuilder(CurrencyConverterInterface::class)
            ->getMock();

        $currencyConverter->expects($this->exactly(2))
            ->method('convert')
            ->with(10)
            ->will($this->returnValue(100));

        $repository = new AdvertRepository($gateway, $currencyConverter);

        $ad = $repository->get(1, AdvertRepository::SOURCE_MYSQL);
        $this->assertArrayHasKey('price', $ad);
        $this->assertEquals(100, $ad['price']);
        $this->assertArrayHasKey('id', $ad);
        $this->assertEquals(1, $ad['id']);

        $ad = $repository->get(2, AdvertRepository::SOURCE_DAEMON);
        $this->assertArrayHasKey('price', $ad);
        $this->assertEquals(100, $ad['price']);
        $this->assertArrayHasKey('id', $ad);
        $this->assertEquals(2, $ad['id']);
    }
}

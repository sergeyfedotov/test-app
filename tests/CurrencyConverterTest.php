<?php declare(strict_types=1);

namespace App\Tests;

use App\CurrencyConverter;
use PHPUnit\Framework\TestCase;

class CurrencyConverterTest extends TestCase
{
    public function testShouldConvertCurrency(): void
    {
        $currencyConverter = new CurrencyConverter(10);

        $this->assertEquals(100, $currencyConverter->convert(10));
    }
}

<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../src/lib.php';

use App\AdvertRepository;
use App\AdvertGateway;
use App\CurrencyConverter;
use App\InvalidAdvertSourceException;
use App\Logging;

Logging::$enabled = \filter_var($_ENV['APP_LOGGING_ENABLED'] ?? false, FILTER_VALIDATE_BOOLEAN);

$repository = new AdvertRepository(
    new AdvertGateway(new \FileLogger('/path/to/file')),
    new CurrencyConverter(65)
);

if (!isset($_GET['id'])) {
    print '<h1>ID не указан</h1>';
    exit;
}

if (!isset($_GET['from'])) {
    print '<h1>Истоник данных не указан</h1>';
    exit;
}

try {
    $ad = $repository->get((int)$_GET['id'], $_GET['from']);

    print '<h1>' . \htmlspecialchars($ad['name']) . '</h1>';
    print '<p>' . \htmlspecialchars($ad['text']) . '</p>';
    print '<p>' . \number_format($ad['price'], 2, ',', ' ') . '</p>';
} catch (InvalidAdvertSourceException $ex) {
    print '<h1>Неизвестный источник данных</h1>';
}

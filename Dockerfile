FROM php:7.2-fpm-alpine3.7

RUN curl -sS https://getcomposer.org/installer \
        | php -- --install-dir=/usr/local/bin/ --filename=composer

WORKDIR /source/

COPY composer.json composer.lock ./

RUN composer -q -n install

COPY public/ public/
COPY src/ src/

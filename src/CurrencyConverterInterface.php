<?php declare(strict_types=1);

namespace App;

interface CurrencyConverterInterface
{
    public function convert(float $amount): float;
}

<?php declare(strict_types=1);

namespace App;

final class AdvertRepository
{
    public const SOURCE_MYSQL = 'Mysql';
    public const SOURCE_DAEMON = 'Daemon';

    private $gateway;
    private $currencyConverter;

    public function __construct(
        AdvertGateway $gateway,
        CurrencyConverterInterface $currencyConverter
    ) {
        $this->gateway = $gateway;
        $this->currencyConverter = $currencyConverter;
    }

    public function get(int $id, string $from): array
    {
        switch ($from) {
            case self::SOURCE_MYSQL:
                return $this->convertAdvert($this->gateway->getFromDb($id));
            case self::SOURCE_DAEMON:
                return $this->convertAdvert($this->gateway->getFromDaemon($id));
        }

        throw new InvalidAdvertSourceException('Unknown source: ' . $from);
    }

    private function convertAdvert($advert): array
    {
        $advert['price'] = $this->currencyConverter->convert($advert['price']);

        return $advert;
    }
}

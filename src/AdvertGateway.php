<?php declare(strict_types=1);

namespace App;

class AdvertGateway
{
    private $logger;

    public function __construct(\FileLogger $logger)
    {
        $this->logger = $logger;
    }

    public function getFromDb(int $id): array
    {
        if (Logging::$enabled) {
            $this->logger->log(\sprintf('[%s] getAdRecord(ID=%d)', \time(), $id));
        }

        return \getAdRecord($id);
    }

    public function getFromDaemon(int $id): array
    {
        if (Logging::$enabled) {
            $this->logger->log(\sprintf('[%s] get_deamon_ad_info(ID=%d)', \time(), $id));
        }

        $record = \explode("\t", \get_deamon_ad_info($id));

        return [
            'id' => (int)$record[0],
            'name' => $record[3],
            'text' => $record[4],
            'price' => (float)$record[5],
        ];
    }
}

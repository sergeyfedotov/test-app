<?php declare(strict_types=1);

namespace App;

class InvalidAdvertSourceException extends Exception {}

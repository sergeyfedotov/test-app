<?php declare(strict_types=1);

namespace App;

final class CurrencyConverter implements CurrencyConverterInterface
{
    private $exchangeRate;

    public function __construct(float $exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }

    public function convert(float $amount): float
    {
        return $amount * $this->exchangeRate;
    }
}
